﻿using FluentAssertions;
using NUnit.Framework;

namespace FizzBuzz.Tests
{
	public class FizzBuzzNumberMapperTests
	{
		[SetUp]
		public void SetUp()
		{
			_mapper = new FizzBuzzNumberMapper();
		}

		[TestCase(1)]
		[TestCase(2)]
		public void Map_IfNumberHasNotMultiply_ReturnsNumber(int number)
		{
			var expectedResult = number.ToString();

			var result = _mapper.Map(number);

			result.Should().Be(expectedResult);
		}

		[TestCase(3)]
		[TestCase(6)]
		public void Map_IfNumberMultiplesOfTree_ReturnsFizz(int number)
		{
			const string expectedResult = "Fizz";

			var result = _mapper.Map(number);

			result.Should().Be(expectedResult);
		}

		[TestCase(5)]
		[TestCase(10)]
		public void Map_IfNumberMultiplesOfFive_ReturnsBuzz(int number)
		{
			const string expectedResult = "Buzz";

			var result = _mapper.Map(number);

			result.Should().Be(expectedResult);
		}

		[TestCase(15)]
		[TestCase(30)]
		public void Map_IfNumberMultiplesOfTreeAndFive_Returns_FizzBuzz(int number)
		{
			const string expectedResult = "FizzBuzz";

			var result = _mapper.Map(number);

			result.Should().Be(expectedResult);
		}

		private FizzBuzzNumberMapper _mapper;
	}
}