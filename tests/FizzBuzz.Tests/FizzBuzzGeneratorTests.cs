using System;
using FakeItEasy;
using FluentAssertions;
using NUnit.Framework;

namespace FizzBuzz.Tests
{
	public class FizzBuzzGeneratorTests
	{
		[SetUp]
		public void Setup()
		{
			_mapper = A.Fake<IFizzBuzzNumberMapper>();
			_generator = new FizzBuzzGenerator(_mapper);
		}

		[Test]
		public void Generate_ReturnsAllAvailableMessageTypes()
		{
			A.CallTo(() => _mapper.Map(1)).Returns("foo");
			A.CallTo(() => _mapper.Map(2)).Returns("bar");
			A.CallTo(() => _mapper.Map(3)).Returns("buz");

			var result = _generator.Generate(1, 3);

			result.Should().BeEquivalentTo("foo", "bar", "buz");
		}

		[Test]
		public void Generate_IfFromIsGreaterThanTo_ThrowsArgumentException()
		{
			Action generate = () => _generator.Generate(2, 1);

			generate.Should().ThrowExactly<ArgumentException>()
				.WithMessage("2 is greater than 1");
		}

		[Test]
		public void Generate_IfFromIsLessThanZero_ThrowsArgumentException()
		{
			Action generate = () => _generator.Generate(-1, 1);

			generate.Should().ThrowExactly<ArgumentException>()
				.WithMessage("-1 cannot be less than 0");
		}

		private FizzBuzzGenerator _generator;
		private IFizzBuzzNumberMapper _mapper;
	}
}