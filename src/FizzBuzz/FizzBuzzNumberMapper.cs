﻿using System.Text;

namespace FizzBuzz
{
	public class FizzBuzzNumberMapper : IFizzBuzzNumberMapper
	{
		public string Map(int number)
		{
			var builder = new StringBuilder();

			if (IsMultipleOfTree(number))
				builder.Append(MultipleOfTreeMessage);
			if (IsMultipleOfFive(number))
				builder.Append(MultipleOfFiveMessage);
			if (builder.Length == 0)
				builder.Append(number.ToString());

			return builder.ToString();
		}

		private static bool IsMultipleOfTree(int number)
		{
			return number % 3 == 0;
		}

		private static bool IsMultipleOfFive(int number)
		{
			return number % 5 == 0;
		}


		private const string MultipleOfTreeMessage = "Fizz";
		private const string MultipleOfFiveMessage = "Buzz";
	}
}