﻿using System.Collections;
using System.Collections.Generic;

namespace FizzBuzz
{
	public interface IFizzBuzzGenerator
	{
		IEnumerable<string> Generate(int from, int to);
	}
}