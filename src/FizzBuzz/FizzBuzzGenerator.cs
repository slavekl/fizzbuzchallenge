﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz
{
	public class FizzBuzzGenerator : IFizzBuzzGenerator
	{
		public FizzBuzzGenerator(IFizzBuzzNumberMapper mapper)
		{
			_mapper = mapper;
		}

		public IEnumerable<string> Generate(int from, int to)
		{
			if (from > to)
				throw new ArgumentException($"{from} is greater than {to}");
			if (from < 0)
				throw new ArgumentException($"{from} cannot be less than 0");

			return Enumerable.Range(from, to).Select(_mapper.Map);
		}

		private readonly IFizzBuzzNumberMapper _mapper;
	}
}
