﻿namespace FizzBuzz
{
	public interface IFizzBuzzNumberMapper
	{
		string Map(int number);
	}
}