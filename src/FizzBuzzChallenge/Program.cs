﻿using System;
using FizzBuzz;

namespace FizzBuzzChallenge
{
	class Program
	{
		static void Main(string[] args)
		{
			var mapper = new FizzBuzzNumberMapper();
			var generator = new FizzBuzzGenerator(mapper);

			var result = generator.Generate(1, 100);

			Console.WriteLine(string.Join(", ", result));
		}
	}
}
